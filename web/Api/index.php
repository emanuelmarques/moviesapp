<?php

//process client requesst
include ("ApiFunctions.php");
header("Content-Type:application/json");
if(!empty($_GET['search'])){
    $searchText = $_GET['search'];
    $response = getSearchResults($searchText);
    deliver_response(200, "valid", $response);


}else{
    //throw invalid
    deliver_response(400, "invalid Request", NULL);
}

function deliver_response($status, $status_message,$data) {
    header("HTTP/1.1 $status $status_message");

    $response['status']= $status;
    $response['status_message'] = $status_message;
    $response['data']=$data;

    $json_response = json_encode($response);
    echo $json_response;
}
?>
