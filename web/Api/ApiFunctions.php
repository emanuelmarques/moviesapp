<?php
function getJsonMovies($search){
    $movies_url = 'https://'.
    'omdbapi.com/'.
    '?s='. urlencode($search);

    $movies_json =  file_get_contents($movies_url);
    $movies_json =  file_get_contents($movies_url);

    return $movies_json;

}

function getSpecificMovie($imdb){
    $movies_url = 'https://'.
    'omdbapi.com/'.
    '?i='. urlencode($imdb).'&plot=full&r=json';

    $movies_json =  file_get_contents($movies_url);
    $movies_json =  file_get_contents($movies_url);
    $movies_array = json_decode($movies_json, true);

    return $movies_array;
}

function getTrailerFromMovie($title)
{
    $key='AIzaSyDV-YVgrNfGMbSvlWoghgk121fG4L45900';

    $baseUrl= 'https://www.youtube.com/embed/';
    $trailers_url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&q='.urlencode($title).'%20trailer&key='.$key;
    $trailers_json =  file_get_contents($trailers_url);

    $trailers_array = json_decode($trailers_json, true);
    $return[0]= $baseUrl . $trailers_array['items'][0]['id']['videoId'];
    $return[1]= 'https://www.youtube.com/watch?v='. $trailers_array['items'][0]['id']['videoId'];
    return $return;

}
function getThumbnailUrl($title){
    $key='AIzaSyDV-YVgrNfGMbSvlWoghgk121fG4L45900';
    $trailers_url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&q='.urlencode($title).'%20trailer&key='.$key;
    $trailers_json =  file_get_contents($trailers_url);

    $trailers_array = json_decode($trailers_json, true);
    return $trailers_array['items'][0]['snippet']['thumbnails']['high']['url'];
}

function getSearchResults($search){


    $movies_json = getJsonMovies($search);
    $movies_array = json_decode($movies_json, true);


    for ($i=0; $i < count($movies_array['Search']); $i++) {
        if($movies_array['Search'][$i]['Type']=='movie'){
            $movie_info = getSpecificMovie($movies_array['Search'][$i]['imdbID']);
            $youtube = getTrailerFromMovie( $movies_array['Search'][$i]['Title'], $movies_array['Search'][$i]['Year']);

            $response_array[$i]['poster']= $movies_array['Search'][$i]['Poster'];
            $response_array[$i]['title']=$movies_array['Search'][$i]['Title'];
            $response_array[$i]['year']=$movies_array['Search'][$i]['Year'];
            $response_array[$i]['genre']=$movie_info['Genre'];
            $response_array[$i]['director']=$movie_info['Director'];
            $response_array[$i]['actors']=$movie_info['Actors'];
            $response_array[$i]['plot']=$movie_info['Plot'];
            $response_array[$i]['country']=$movie_info['Country'];
            $response_array[$i]['imdb_rating']=$movie_info['imdbRating'];
            $response_array[$i]['trailer']= $youtube[1];



        }
    }
    return $response_array;


}


?>
