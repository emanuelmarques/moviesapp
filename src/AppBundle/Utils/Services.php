<?php
namespace AppBundle\Utils;





class Services
{

    public static  function getJsonMovies($search){
        $movies_url = 'https://'.
        'omdbapi.com/'.
        '?s='. urlencode($search);
        $movies_json =  file_get_contents($movies_url);
        $movies_json =  file_get_contents($movies_url);

        return $movies_json;

    }

    public static function getSpecificMovie($imdb){
        $movies_url = 'https://'.
        'omdbapi.com/'.
        '?i='. urlencode($imdb).'&plot=full&r=json';
        $movies_json =  file_get_contents($movies_url);
        $movies_json =  file_get_contents($movies_url);
        $movies_array = json_decode($movies_json, true);

        return $movies_array;
    }

    public static function getTrailerFromMovie($title)
    {
        $key='AIzaSyDV-YVgrNfGMbSvlWoghgk121fG4L45900';

        $baseUrl= 'https://www.youtube.com/embed/';
        $trailers_url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&q='.urlencode($title).'%20trailer&key='.$key;
        $trailers_json =  file_get_contents($trailers_url);

        $trailers_array = json_decode($trailers_json, true);
        $return[0]= $baseUrl . $trailers_array['items'][0]['id']['videoId'];
        $return[1]= 'https://www.youtube.com/watch?v='. $trailers_array['items'][0]['id']['videoId'];
        return $return;

    }
    public static function getThumbnailUrl($title){
        $key='AIzaSyDV-YVgrNfGMbSvlWoghgk121fG4L45900';
        $trailers_url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&q='.urlencode($title).'%20trailer&key='.$key;
        $trailers_json =  file_get_contents($trailers_url);

        $trailers_array = json_decode($trailers_json, true);
        return $trailers_array['items'][0]['snippet']['thumbnails']['high']['url'];
    }

}

?>
