<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Utils\Services;


use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{


    /**
    * @Route("/", name="homepage")
    */
    public function indexAction(Request $request)
    {

        $title = 'Movies';

        // if (!empty($_GET['searchField'])) {
        if (!empty($_GET['searchField'])){
            $movies_json = Services::getJsonMovies($_GET['searchField']);
            $movies_array = json_decode($movies_json, true);
            $cache = 'cache/'.urlencode($_GET['searchField']) .'.cache';

        }else {
            $movies_array = null;
            $cache = null;
        }

        if( file_exists($cache)){
            include ($cache);
            $string = '';
        }else{
            $string ='

            <script type="text/javascript" src="https://moodleant.isep.ipp.pt/lib/ufo.js"></script>
            <link rel="stylesheet" type="text/css"  href="https://moodleant.isep.ipp.pt/theme/standard/styles.php" >
            <link rel="stylesheet" type="text/css"  href="https://moodleant.isep.ipp.pt/theme/standard/styles.php" >
            <link rel="stylesheet" type="text/css"  href="https://moodleant.isep.ipp.pt/theme/ISEP_Novo/styles.php" >
            <title>'.$title.'</title>
            <center><h1>'.$title.'</h1></center>

            <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : \'509408622566076\',
                    xfbml      : true,
                    version    : \'v2.5\'
                });
            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, \'script\', \'facebook-jssdk\'));
            </script>
            <center><form method="get">
            <input width="50"  type="text" name="searchField"/>
            <button  type="submit">Search</button>
            </form></center>
            </p>
            <table border="0" cellpadding="1" style="width:100%">';

            for ($i=0; $i < count($movies_array['Search']); $i++) {
                if($movies_array['Search'][$i]['Type']=='movie'){
                    $movie_info = Services::getSpecificMovie($movies_array['Search'][$i]['imdbID']);
                    $youtube = Services::getTrailerFromMovie( $movies_array['Search'][$i]['Title'], $movies_array['Search'][$i]['Year']);
                    $string .=
                    '<tr>

                    <td><center><img src=\''. $movies_array['Search'][$i]['Poster'].'\'/></center> </td>

                    <td><center> <b>Title: </b>'. $movies_array['Search'][$i]['Title'].' </center>
                    <center><b>Release Year: </b>'. $movies_array['Search'][$i]['Year'].' </center>
                    <center><b>Genre: </b>'. $movie_info['Genre'].'</center>
                    <center><b>Director: </b>'. $movie_info['Director'].'</center>
                    <center><b>Actors: </b>'. $movie_info['Actors'].'</center>
                    <center><b>Plot: </b>'. $movie_info['Plot'].'</center>
                    <center><b>Country: </b>'. $movie_info['Country'].'</center>
                    <center><b>IMDb Rating: </b>'. $movie_info['imdbRating'].'</center>


                    <p/><center><div class="fb-share-button" data-href="'.$youtube[1].'" data-layout="button_count"></div></center></td>

                    <td><center>
                    <iframe width="420" height="315"
                    src="'.$youtube[0].'">
                    </iframe>
                    </center></td>


                    </tr>';
                }
            }

            $string .='</table>';
            if (!empty($_GET['searchField'])){

                $fh = fopen($cache, 'w+') or die ('error writing file');
                fwrite($fh, $string);
                fclose($fh);
            }
        }
        return new Response($string);
    }
}
?>
